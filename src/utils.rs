use crate::error::{Error, Result};

use std::io::Cursor;
use std::{collections::HashMap, str::FromStr};

use atrium_api::types::TryIntoUnknown;
use atrium_api::{
    agent::{store::MemorySessionStore, AtpAgent, Session},
    app::bsky,
    app::bsky::feed::post::Record,
    client::AtpServiceClient,
    com::atproto,
    types::{
        string::{AtIdentifier, Did, Handle, Nsid},
        BlobRef, LimitedNonZeroU8, TryFromUnknown,
    },
    xrpc::{
        http::{uri::Builder, Request, Response},
        types::AuthorizationToken,
        HttpClient, XrpcClient,
    },
};
use atrium_xrpc_client::reqwest::ReqwestClient;
use image::{imageops, GenericImageView, ImageFormat, ImageReader};
use ipld_core::ipld::Ipld::Null;
use rand::seq::SliceRandom;
use reqwest::header;
use serde::Serialize;
use tokio::io::AsyncReadExt;
use tokio::process::Command;
use tokio::time::{sleep, Duration};

const VIDEO_SERVICE: &str = "https://video.bsky.app";
const VIDEO_SERVICE_DID: &str = "did:web:video.bsky.app";
const UPLOAD_VIDEO_PATH: &str = "/xrpc/app.bsky.video.uploadVideo";

pub async fn fetch_gif(url: &str) -> Result<Vec<u8>> {
    log::info!("fetch_fig");

    let client = reqwest::Client::new();
    let user_agent = [
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.3",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.3",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.1",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.4.1 Safari/605.1.1",
        ].choose(&mut rand::thread_rng()).ok_or(Error::Any("Cannot select user agent".into()))?.to_owned();

    let response = client
        .get(url)
        .header(header::USER_AGENT, user_agent)
        .send()
        .await?;

    let bytes = response.bytes().await?;
    let mut cursor = Cursor::new(bytes);

    let mut file = std::fs::File::create("temp.gif")?;
    std::io::copy(&mut cursor, &mut file)?;

    log::info!("Convert gif to mp4 by ffmpeg");
    let status = Command::new("ffmpeg")
        .args([
            "-i",
            "temp.gif",
            "-pix_fmt",
            "yuv420p",
            "-metadata",
            &format!("title={}", url),
            "temp.mp4",
            "-y",
        ])
        .status()
        .await?;

    if !status.success() {
        return Err(Error::Any(format!(
            "Conversion failed with status: {}",
            status
        )));
    }
    let mut file = tokio::fs::File::open("temp.mp4").await?;
    let mut buffer = Vec::new();
    file.read_to_end(&mut buffer).await?;
    Ok(buffer)
}

#[allow(async_fn_in_trait)]
pub trait BlueskyBot {
    async fn execute(&mut self, dry_run: bool) -> Result<()>;

    fn handle(&self) -> &str;
    fn password(&self) -> &str;
    fn session(&self) -> &Option<Session>;
    fn session_mut(&mut self) -> &mut Option<Session>;
    fn sleep_after_post_in_seconds(&self) -> u64 {
        10
    }
    fn sleep_after_fetch_in_seconds(&self) -> u64 {
        5
    }
    async fn get_agent(&mut self) -> Result<AtpAgent<MemorySessionStore, ReqwestClient>> {
        let agent = AtpAgent::new(
            ReqwestClient::new("https://bsky.social"),
            MemorySessionStore::default(),
        );
        if let Some(session) = self.session() {
            if agent.resume_session(session.clone()).await.is_ok() {
                return Ok(agent);
            }
        }
        let response = match agent.login(self.handle(), self.password()).await {
            Ok(session) => {
                *self.session_mut() = Some(session);
                Ok(agent)
            }
            Err(err) => {
                log::error!("{:?}", self.handle());
                log::error!("{:?}", err);
                Err(err)
            }
        };
        Ok(response?)
    }

    async fn fetch_posted_records(
        &mut self,
        limit: usize,
    ) -> Result<Vec<bsky::feed::post::Record>> {
        let agent = self.get_agent().await?;

        let mut count = 0;
        let mut cursor = None;
        let mut records = Vec::new();

        while let Ok(response) = agent
            .api
            .app
            .bsky
            .feed
            .get_author_feed(bsky::feed::get_author_feed::Parameters {
                data: bsky::feed::get_author_feed::ParametersData {
                    actor: AtIdentifier::Handle(
                        Handle::new(self.handle().to_string())
                            .map_err(|e| Error::Any(e.to_string()))?,
                    ),
                    cursor: cursor.take(),
                    filter: None,
                    limit: LimitedNonZeroU8::try_from(100).ok(),
                    include_pins: Some(false),
                },
                extra_data: Null,
            })
            .await
        {
            for feed in response.feed.clone() {
                if let Ok(record) = Record::try_from_unknown(feed.data.post.data.record) {
                    records.push(record);
                }
            }
            cursor.clone_from(&response.cursor);
            let size = response.feed.len();
            count += size;
            log::info!("{} fetched {} posts.", self.handle(), count);
            if cursor.is_none() || size == 0 || count >= limit {
                break;
            }
            sleep(Duration::from_secs(self.sleep_after_fetch_in_seconds())).await;
        }
        Ok(records)
    }

    async fn post(&mut self, record: Record, dry_run: bool) -> Result<()> {
        let agent = self.get_agent().await?;
        let repo = AtIdentifier::Did(
            self.session()
                .clone()
                .ok_or(Error::Any("Empty session".into()))?
                .did
                .clone(),
        );
        let input = atproto::repo::create_record::Input {
            data: atproto::repo::create_record::InputData {
                collection: Nsid::from_str("app.bsky.feed.post")
                    .map_err(|e| Error::Any(e.to_string()))?,
                record: record.try_into_unknown()?,
                repo,
                rkey: None,
                swap_commit: None,
                validate: None,
            },
            extra_data: Null,
        };
        log::info!("create_record({:?})", input);
        if !dry_run {
            agent.api.com.atproto.repo.create_record(input).await?;
            sleep(Duration::from_secs(self.sleep_after_post_in_seconds())).await;
        }
        Ok(())
    }

    async fn upload_image(&mut self, url: &str) -> Result<BlobRef> {
        let agent = self.get_agent().await?;

        let bytes = reqwest::get(url).await?.bytes().await?;
        let img = ImageReader::new(Cursor::new(bytes))
            .with_guessed_format()?
            .decode()?;

        // TODO: dynamic reshaping
        let (width_u, height_u) = img.dimensions();
        let width = f64::from(width_u);
        let height = f64::from(height_u);
        let factor = (400.0 / width).max(300.0 / height);
        let new_width = (width * factor) as u32;
        let new_height = (height * factor) as u32;
        let thumbnail = imageops::thumbnail(&img, new_width, new_height);

        let mut buffer: Vec<u8> = Vec::new();
        thumbnail.write_to(&mut Cursor::new(&mut buffer), ImageFormat::Png)?;

        let response = agent.api.com.atproto.repo.upload_blob(buffer).await?;
        Ok(response.blob.to_owned())
    }

    async fn upload_video(&mut self, url: &str) -> Result<BlobRef> {
        let agent = self.get_agent().await?;

        let limits = {
            let service_auth = agent
                .api
                .com
                .atproto
                .server
                .get_service_auth(
                    atrium_api::com::atproto::server::get_service_auth::ParametersData {
                        aud: VIDEO_SERVICE_DID.parse().expect("invalid DID"),
                        exp: None,
                        lxm: atrium_api::app::bsky::video::get_upload_limits::NSID
                            .parse()
                            .ok(),
                    }
                    .into(),
                )
                .await
                .map_err(|e| {
                    Error::Any(format!("Failed to get SessionAuth for UploadLimit: {}", e))
                })?;
            let client = AtpServiceClient::new(VideoClient::new(service_auth.data.token, None));
            client
                .service
                .app
                .bsky
                .video
                .get_upload_limits()
                .await
                .map_err(|e| Error::Any(format!("Failed to get UploadLimit: {}", e)))?
        };
        let data = fetch_gif(url).await?;
        if !limits.can_upload
            || limits
                .remaining_daily_bytes
                .map_or(false, |remain| remain < data.len() as i64)
            || limits
                .remaining_daily_videos
                .map_or(false, |remain| remain <= 0)
        {
            return Err(Error::Any(format!(
                "You cannot upload a video: {:?}",
                limits.data
            )));
        }
        let output = {
            let service_auth = agent
                .api
                .com
                .atproto
                .server
                .get_service_auth(
                    atrium_api::com::atproto::server::get_service_auth::ParametersData {
                        aud: format!(
                            "did:web:{}",
                            agent.get_endpoint().await.strip_prefix("https://").unwrap()
                        )
                        .parse()
                        .expect("invalid DID"),
                        exp: None,
                        lxm: atrium_api::com::atproto::repo::upload_blob::NSID
                            .parse()
                            .ok(),
                    }
                    .into(),
                )
                .await
                .map_err(|e| Error::Any(format!("Failed to get ServiceAuth: {}", e)))?;

            let filename = "data.mp4".to_string();
            let client = AtpServiceClient::new(VideoClient::new(
                service_auth.data.token,
                Some(UploadParams {
                    did: agent
                        .get_session()
                        .await
                        .ok_or(Error::Any("session is empty".to_string()))?
                        .did
                        .clone(),
                    name: filename,
                }),
            ));
            client
                .service
                .app
                .bsky
                .video
                .upload_video(data)
                .await
                .map_err(|e| Error::Any(format!("Failed to upload: {:?}", e)))?
        };

        let client = AtpServiceClient::new(ReqwestClient::new(VIDEO_SERVICE));
        let mut status = output.data.job_status.data;
        loop {
            status = client
                .service
                .app
                .bsky
                .video
                .get_job_status(
                    atrium_api::app::bsky::video::get_job_status::ParametersData {
                        job_id: status.job_id.clone(),
                    }
                    .into(),
                )
                .await
                .map_err(|e| Error::Any(format!("Failed to get the job status {:?}", e)))?
                .data
                .job_status
                .data;
            if status.blob.is_some() || status.state == "JOB_STATE_FAILED" {
                break;
            }
            sleep(Duration::from_millis(100)).await;
        }
        if let Some(video) = &status.blob {
            Ok(video.to_owned())
        } else {
            Err(Error::Any(format!("Failed to get blob: {:?}", status)))
        }
    }
}

pub async fn fetch(url: &str, scrappey_key: Option<&str>) -> Result<String> {
    let client = reqwest::Client::new();

    let response = if let Some(key) = scrappey_key {
        log::info!("Fetch {} using Scrappey", url);
        let endpoint = format!("https://publisher.scrappey.com/api/v1?key={key}");
        let mut params = HashMap::new();
        params.insert("cmd", "request.get");
        params.insert("url", url);
        let response = client.post(endpoint).json(&params).send().await?;
        let body = response.text().await?;
        let value: serde_json::Value = serde_json::from_str(&body)?;
        value
            .get("solution")
            .ok_or(Error::Scrappey(format!(
                "Response from Scrappey for {url} has no solution: {value:?}"
            )))?
            .get("response")
            .ok_or(Error::Scrappey(format!(
                "Response from Scrappey for {url} has no response: {value:?}"
            )))?
            .as_str()
            .ok_or(Error::Scrappey(format!(
                "Response from Scrappey for {url} has invalid response type: {value:?}"
            )))?
            .to_owned()
    } else {
        log::info!("Fetch {}", url);
        let user_agent = [
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.3",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.3",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.1",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.4.1 Safari/605.1.1",
    ].choose(&mut rand::thread_rng()).ok_or(Error::Any("Cannot select user agent".to_string()))?.to_owned();
        let response = client
            .get(url)
            .header(header::USER_AGENT, user_agent)
            .send()
            .await?;
        response.text().await?
    };
    Ok(response)
}

#[derive(Serialize)]
struct UploadParams {
    did: Did,
    name: String,
}

struct VideoClient {
    token: String,
    params: Option<UploadParams>,
    inner: ReqwestClient,
}

impl VideoClient {
    fn new(token: String, params: Option<UploadParams>) -> Self {
        Self {
            token,
            params,
            inner: ReqwestClient::new(
                // Actually, `base_uri` returns `VIDEO_SERVICE`, so there is no need to specify this.
                "https://dummy.example.com",
            ),
        }
    }
}

impl HttpClient for VideoClient {
    async fn send_http(
        &self,
        mut request: Request<Vec<u8>>,
    ) -> std::result::Result<Response<Vec<u8>>, Box<dyn std::error::Error + Send + Sync + 'static>>
    {
        let is_upload_video = request.uri().path() == UPLOAD_VIDEO_PATH;
        // Hack: Append query parameters
        if is_upload_video {
            if let Some(params) = &self.params {
                *request.uri_mut() = Builder::from(request.uri().clone())
                    .path_and_query(format!(
                        "{UPLOAD_VIDEO_PATH}?{}",
                        serde_html_form::to_string(params)?
                    ))
                    .build()?;
            }
        }
        let mut response = self.inner.send_http(request).await;
        // Hack: Formatting an incorrect response body
        if is_upload_video {
            if let Ok(res) = response.as_mut() {
                *res.body_mut() = [
                    b"{\"jobStatus\":".to_vec(),
                    res.body().to_vec(),
                    b"}".to_vec(),
                ]
                .concat();
            }
        }
        response
    }
}

impl XrpcClient for VideoClient {
    fn base_uri(&self) -> String {
        VIDEO_SERVICE.to_string()
    }
    async fn authorization_token(&self, _: bool) -> Option<AuthorizationToken> {
        Some(AuthorizationToken::Bearer(self.token.clone()))
    }
}
