#![allow(unused)]

use std::collections::HashSet;
use std::io::Cursor;

use crate::error::{Error, Result};
use crate::utils::{fetch, BlueskyBot};
use atrium_api::agent::Session;
use atrium_api::app::bsky;
use atrium_api::types::BlobRef;
use atrium_api::types::{string::Datetime, Union};
use bsky_sdk::rich_text::RichText;
use feed_rs::parser as feed_parser;
use ipld_core::ipld::Ipld::Null;
use rand::seq::SliceRandom;
use regex::Regex;
use reqwest::header;
use scraper::{Html, Selector};
use tokio::io::AsyncReadExt;
use tokio::process::Command;

pub struct ArxivBot {
    handle: String,
    password: String,
    filter: String,
    session: Option<Session>,
    limit: usize,
    url: String,
    scrappey_key: Option<String>,
    posted: HashSet<String>,
}

impl BlueskyBot for ArxivBot {
    fn handle(&self) -> &str {
        self.handle.as_str()
    }
    fn password(&self) -> &str {
        self.password.as_str()
    }
    fn session(&self) -> &Option<Session> {
        &self.session
    }
    fn session_mut(&mut self) -> &mut Option<Session> {
        &mut self.session
    }
    fn sleep_after_post_in_seconds(&self) -> u64 {
        30
    }
    async fn execute(&mut self, dry_run: bool) -> Result<()> {
        self.execute_impl(dry_run).await
    }
}

pub struct Article {
    pub authors: Vec<String>,
    pub title: String,
    pub url: String,
    pub is_new: bool,
}

// Invariant
//    self.key() == Self::from(self.into()).key()
impl Article {
    pub fn key(&self) -> String {
        self.url.to_owned()
    }
    pub fn with(record: &bsky::feed::post::Record) -> Result<Self> {
        let lines = record.text.split("\n").collect::<Vec<_>>();
        for line in lines {
            if line.starts_with("http") {
                return Ok(Article {
                    authors: vec![],
                    title: "".to_string(),
                    url: line.to_string(),
                    is_new: false,
                });
            }
        }
        Err(Error::Any("Cannot find URL from the post".to_string()))
    }
    pub fn authors_text(&self) -> Result<String> {
        match self.authors.len() {
            0 => Err(Error::Any("authors are empty".to_string())),
            1 => Ok(self.authors[0].clone()),
            2 => Ok(format!("{}, {}", self.authors[0], self.authors[1])),
            _ => Ok(format!("{}, {}, ...", self.authors[0], self.authors[1])),
        }
    }
    pub fn get_text(&self) -> Result<String> {
        let limit = 300;

        let mut author = self.authors.join(",");
        let mut title = self.title.clone();
        let url = self.url.clone();

        // no treatment
        let mut text = format!("{}\n{}\n{}", author, title, url);
        if text.len() <= limit {
            return Ok(text);
        }
        // just truncate authors
        if self.authors.len() >= 2 {
            for k in (1..self.authors.len() - 1).rev() {
                author = format!(
                    "{}, ...",
                    self.authors
                        .iter()
                        .take(k)
                        .cloned()
                        .collect::<Vec<_>>()
                        .join(", ")
                );
                text = format!("{}\n{}\n{}", author, title, url);
                if text.len() <= limit {
                    return Ok(text);
                }
            }
        }
        let remain = text.len() - limit - 3;
        title = title.chars().take(remain).collect();
        text = format!("{}\n{}...\n{}", author, title, url);

        assert!(text.len() <= limit);
        Ok(text)
    }
}

impl ArxivBot {
    pub fn new(
        handle: String,
        password: String,
        url: String,
        filter: String,
        limit: usize,
    ) -> Self {
        Self {
            handle,
            password,
            url,
            filter,
            scrappey_key: None,
            limit,
            session: None,
            posted: HashSet::new(),
        }
    }
    pub async fn fetch_articles(&self) -> Result<Vec<Article>> {
        let rss = fetch(self.url.as_ref(), self.scrappey_key.as_deref()).await?;
        let feed = feed_parser::parse(rss.as_bytes())?;

        let mut articles = Vec::new();
        for entry in feed.entries {
            if entry.categories[0].term != self.filter {
                log::info!("Skipped since its a cross post");
                continue;
            }
            let is_new = entry.id.ends_with("v1");
            let url = entry.links.first().unwrap().href.clone();

            let authors_text = entry
                .authors
                .iter()
                .map(|person| person.name.clone())
                .collect::<Vec<_>>()
                .join(",");
            let re = Regex::new(r#"</?("[^"]*"|'[^']*'|[^>])*(>|$)"#).unwrap();
            let format = move |s| {
                re.replace_all(s, "")
                    .replace("&amp;", "&")
                    .replace("&lt;", "<")
                    .replace("&gt;", ">")
                    .replace("&quot;", "\"")
                    .replace("&apos;", "'")
            };
            let authors = format(&authors_text)
                .split(",")
                .map(|s| s.to_string())
                .collect::<Vec<_>>();
            let title = entry.title.unwrap().content.clone();

            if is_new {
                articles.push(Article {
                    authors,
                    title,
                    url,
                    is_new,
                });
            }
        }
        articles.reverse();
        Ok(articles)
    }
    pub async fn build_record(&mut self, article: Article) -> Result<bsky::feed::post::Record> {
        let text = article.get_text()?;
        let rt = RichText::new_with_detect_facets(text).await.unwrap();

        let record = bsky::feed::post::Record {
            data: bsky::feed::post::RecordData {
                text: rt.text,
                facets: rt.facets,
                created_at: Datetime::now(),
                embed: None,
                entities: None,
                langs: None,
                reply: None,
                labels: None,
                tags: None,
            },
            extra_data: Null,
        };
        Ok(record)
    }
    pub async fn execute_impl(&mut self, dry_run: bool) -> Result<()> {
        // Fetch previous posts before the execution
        let limit = if self.posted.len() < 100 { 1000 } else { 100 };
        for record in self.fetch_posted_records(limit).await? {
            let key = Article::with(&record)?.key();
            self.posted.insert(key);
        }
        // Fetch articles to be posted
        let mut count = 0;
        for article in self.fetch_articles().await? {
            let key = article.key();
            if self.posted.contains(&key) {
                continue;
            }
            self.posted.insert(key);
            let record = self.build_record(article).await?;
            self.post(record, dry_run).await?;
            count += 1;
            if count >= self.limit {
                break;
            }
        }
        Ok(())
    }
}
