#![allow(unused_variables)]
#![allow(unused_mut)]

extern crate alloc;

use atrium_api::{
    agent::Session,
    app::bsky,
    types::{string::Datetime, Union},
};
use ipld_core::ipld::Ipld::Null;
use regex::Regex;
use scraper::{Html, Selector};

use crate::{
    error::{Error, Result},
    utils::{fetch, BlueskyBot},
};
use std::collections::HashSet;

pub struct CardiffBusBot {
    handle: String,
    password: String,
    session: Option<Session>,
    limit: usize,
    pub posted: HashSet<String>,
}

impl BlueskyBot for CardiffBusBot {
    fn handle(&self) -> &str {
        self.handle.as_str()
    }
    fn password(&self) -> &str {
        self.password.as_str()
    }
    fn session(&self) -> &Option<Session> {
        &self.session
    }
    fn session_mut(&mut self) -> &mut Option<Session> {
        &mut self.session
    }
    async fn execute(&mut self, dry_run: bool) -> Result<()> {
        self.execute_impl(dry_run).await
    }
}

pub struct Article {
    title: String,
    description: String,
    url: String,
    image_url: Option<String>,
}

impl Article {
    pub fn key(&self) -> String {
        self.url.clone()
    }
    pub fn with(record: &bsky::feed::post::Record) -> Result<Self> {
        if let Some(&Union::Refs(bsky::feed::post::RecordEmbedRefs::AppBskyEmbedExternalMain(
            ref main,
        ))) = record.embed.as_ref()
        {
            Ok(Article {
                title: main.data.external.title.to_string(),
                description: "".to_string(),
                url: main.data.external.uri.to_string(),
                image_url: None,
            })
        } else {
            Err(Error::Any("Invalid Record".into()))
        }
    }
}

impl CardiffBusBot {
    pub fn new(handle: String, password: String, limit: usize) -> Self {
        Self {
            handle,
            password,
            limit,
            session: None,
            posted: HashSet::new(),
        }
    }

    pub async fn fetch_travel_updates(&self) -> Result<Vec<Article>> {
        let mut articles = Vec::new();

        const URL: &str = "https://www.cardiffbus.com/travel-updates";
        let html = fetch(URL, None).await?;
        let document = Html::parse_document(&html);

        let selector = Selector::parse("div.disruption-item").unwrap();
        for item in document.select(&selector) {
            let selector = Selector::parse("h3.disruption-item__title").unwrap();
            let entry = item.select(&selector).next().unwrap();

            let id = entry.attr("id").unwrap();
            let title = entry.text().collect::<Vec<_>>().join("").trim().to_string();
            let url = format!("https://www.cardiffbus.com/travel-updates#{}", id);
            let description = {
                let selector = Selector::parse("p:not(.disruption-item__meta)").unwrap();
                item.select(&selector)
                    .next()
                    .unwrap()
                    .text()
                    .collect::<Vec<_>>()
                    .join("")
                    .trim()
                    .to_string()
            };
            articles.push(Article {
                title,
                description,
                url,
                image_url: None,
            });
        }
        Ok(articles)
    }

    async fn fetch_news(&self) -> Result<Vec<Article>> {
        let mut articles = Vec::new();

        const URL: &str = "https://www.cardiffbus.com/news";
        let html = fetch(URL, None).await?;
        let document = Html::parse_document(&html);
        let selector = Selector::parse("div.news-preview").unwrap();
        for item in document.select(&selector) {
            let mut image_url = None;
            let selector = Selector::parse("div.news-preview__image > div.bg-full").unwrap();
            if let Some(element) = item.select(&selector).next() {
                if let Some(style) = element.value().attr("style") {
                    let re = Regex::new(r"url\('?(.*?)'?\)").unwrap();
                    if let Some(captures) = re.captures(style) {
                        if let Some(url) = captures.get(1) {
                            image_url = Some(url.as_str().to_string());
                        }
                    }
                }
            }
            println!("{:?}", image_url);
            let selector = Selector::parse("a.news-preview__title").unwrap();
            let element = item
                .select(&selector)
                .next()
                .ok_or(Error::Any("has no title".to_string()))?;
            let title = element
                .text()
                .collect::<Vec<_>>()
                .join("")
                .trim()
                .to_string();
            let href = element.attr("href").unwrap();
            let url = format!("https://www.cardiffbus.com{}", href);

            let selector =
                Selector::parse("div.news-preview__meta > span.news-preview__date--absolute")
                    .unwrap();
            let element = item
                .select(&selector)
                .next()
                .ok_or(Error::Any("has no absolute date".to_string()))?;
            let date = element
                .text()
                .collect::<Vec<_>>()
                .join("")
                .trim()
                .to_string();

            let selector = Selector::parse("div.news-preview__copy").unwrap();
            let copy = item
                .select(&selector)
                .next()
                .ok_or(Error::Any("has no description".to_string()))?
                .text()
                .collect::<Vec<_>>()
                .join("")
                .trim()
                .to_string();

            let description = format!("{title}\n{date}\n{copy}");
            articles.push(Article {
                title,
                description,
                url,
                image_url,
            });
        }
        Ok(articles)
    }

    pub async fn fetch_articles(&self) -> Result<Vec<Article>> {
        let mut articles = Vec::new();
        articles.extend(self.fetch_news().await?);
        articles.extend(self.fetch_travel_updates().await?);
        articles.reverse();
        Ok(articles)
    }

    pub async fn build_record(&mut self, article: Article) -> Result<bsky::feed::post::Record> {
        let thumb = if let Some(image_url) = &article.image_url {
            self.upload_image(image_url).await.ok()
        } else {
            None
        };
        let embed = Union::Refs(bsky::feed::post::RecordEmbedRefs::AppBskyEmbedExternalMain(
            Box::new(bsky::embed::external::Main {
                data: bsky::embed::external::MainData {
                    external: bsky::embed::external::External {
                        data: bsky::embed::external::ExternalData {
                            description: "".to_string(),
                            thumb,
                            title: article.title.clone(),
                            uri: article.url.clone(),
                        },
                        extra_data: Null,
                    },
                },
                extra_data: Null,
            }),
        ));
        let text = article.description.clone();
        let record = bsky::feed::post::Record {
            data: bsky::feed::post::RecordData {
                text,
                created_at: Datetime::now(),
                embed: Some(embed),
                entities: None,
                facets: None,
                langs: None,
                reply: None,
                labels: None,
                tags: None,
            },
            extra_data: Null,
        };
        Ok(record)
    }

    pub async fn execute_impl(&mut self, dry_run: bool) -> Result<()> {
        let limit = if self.posted.len() < 100 { 1000 } else { 100 };
        for record in self.fetch_posted_records(limit).await? {
            let key = Article::with(&record)?.key();
            self.posted.insert(key);
        }

        // Fetch articles to be posted
        let mut count = 0;
        for article in self.fetch_articles().await? {
            let key = article.key();
            if self.posted.contains(&key) {
                continue;
            }
            self.posted.insert(key);
            let record = self.build_record(article).await?;
            self.post(record, dry_run).await?;
            count += 1;
            if count >= self.limit {
                break;
            }
        }
        Ok(())
    }
}
