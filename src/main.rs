extern crate alloc;

use rand::seq::SliceRandom;
use rand::thread_rng;
use serde::{Deserialize, Serialize};
use std::env;
use std::fs::read_to_string;
use tokio::time::{sleep, timeout, Duration};

use bluesky_newsbots::{
    arxiv::ArxivBot,
    cardiffbus::CardiffBusBot,
    ealing_times::EalingTimesBot,
    error::Result,
    londonist::LondonistBot,
    news_bot::{Exclude, NewsBot},
    nmcc_bot::NmccBot,
    pusheen::PusheenBot,
    utils::BlueskyBot,
};

#[derive(Serialize, Deserialize, Debug, Clone)]
struct NewsBotConfig {
    handle: String,
    password: String,
    limit: Option<usize>,
    rss_url: String,
    scrappey_key: Option<String>,
    exclude: Option<Exclude>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct BotConfig {
    handle: String,
    password: String,
    limit: Option<usize>,
    url: String,
    scrappey_key: Option<String>,
    filter: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Config {
    dry_run: Option<bool>,
    timeout_in_seconds: Option<u64>,
    bots: Option<Vec<NewsBotConfig>>,
    nmcc: Option<BotConfig>,
    londonist: Option<BotConfig>,
    ealing_times: Option<BotConfig>,
    cardiffbus: Option<BotConfig>,
    pusheen: Option<BotConfig>,
    arxiv: Option<Vec<BotConfig>>,
}

enum Bot {
    News(NewsBot),
    Londonist(LondonistBot),
    EalingTimes(EalingTimesBot),
    Pusheen(PusheenBot),
    Arxiv(ArxivBot),
    Nmcc(NmccBot),
    CardiffBus(CardiffBusBot),
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();
    let config_json = if let Ok(path) = env::var("CONFIG_JSON") {
        log::info!("CONFIG_JSON = {:?}", path);
        path
    } else {
        log::info!("No CONFIG_JSON environemntal variable");
        "config.json".to_string()
    };
    let config: Config = serde_json::from_str(&read_to_string(config_json)?)?;
    let dry_run = config.dry_run.unwrap_or(true);
    let timeout_in_seconds = config.timeout_in_seconds.unwrap_or(60);

    let mut bots: Vec<Bot> = Vec::new();
    if let Some(configs) = config.bots {
        for c in configs {
            let bot = NewsBot::new(
                c.handle.clone(),
                c.password.clone(),
                c.scrappey_key.clone(),
                c.limit.unwrap_or(3),
                c.rss_url.clone(),
                c.exclude.clone().unwrap_or_default(),
            )?;
            bots.push(Bot::News(bot));
        }
    }
    if let Some(config) = &config.nmcc {
        bots.push(Bot::Nmcc(NmccBot::new(
            config.handle.clone(),
            config.password.clone(),
            config.limit.unwrap_or(3),
        )?));
    }
    if let Some(config) = &config.londonist {
        bots.push(Bot::Londonist(LondonistBot::new(
            config.handle.clone(),
            config.password.clone(),
            config.url.clone(),
            config.scrappey_key.clone(),
            config.limit.unwrap_or(3),
        )));
    }
    if let Some(config) = &config.ealing_times {
        bots.push(Bot::EalingTimes(EalingTimesBot::new(
            config.handle.clone(),
            config.password.clone(),
            config.url.clone(),
            config.scrappey_key.clone(),
            config.limit.unwrap_or(3),
        )));
    }
    if let Some(config) = &config.cardiffbus {
        bots.push(Bot::CardiffBus(CardiffBusBot::new(
            config.handle.clone(),
            config.password.clone(),
            config.limit.unwrap_or(5),
        )));
    }
    if let Some(config) = &config.pusheen {
        bots.push(Bot::Pusheen(PusheenBot::new(
            config.handle.clone(),
            config.password.clone(),
            config.url.clone(),
            config.limit.unwrap_or(3),
        )));
    }
    if let Some(configs) = &config.arxiv {
        for config in configs {
            bots.push(Bot::Arxiv(ArxivBot::new(
                config.handle.clone(),
                config.password.clone(),
                config.url.clone(),
                config.filter.clone().unwrap(),
                config.limit.unwrap_or(5),
            )));
        }
    }
    loop {
        bots.shuffle(&mut thread_rng());
        for bot in &mut bots {
            let duration = Duration::from_secs(timeout_in_seconds);
            let e = match bot {
                Bot::Londonist(bot) => timeout(duration, bot.execute(dry_run)).await,
                Bot::EalingTimes(bot) => timeout(duration, bot.execute(dry_run)).await,
                Bot::Pusheen(bot) => timeout(duration, bot.execute(dry_run)).await,
                Bot::Arxiv(bot) => timeout(duration, bot.execute(dry_run)).await,
                Bot::Nmcc(bot) => timeout(duration, bot.execute(dry_run)).await,
                Bot::News(bot) => timeout(duration, bot.execute(dry_run)).await,
                Bot::CardiffBus(bot) => timeout(duration, bot.execute(dry_run)).await,
            };
            match e {
                Ok(Ok(())) => (),
                Ok(Err(e)) => log::error!("{:?}", e),
                Err(_) => log::error!("timeout"),
            };
        }
        sleep(Duration::from_secs(60)).await;
    }
}
