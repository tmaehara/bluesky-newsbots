#![allow(unused_variables)]
#![allow(unused_mut)]

extern crate alloc;

use alloc::borrow::ToOwned;
use atrium_api::{
    agent::{store::MemorySessionStore, AtpAgent, Session},
    app::bsky::{self, feed},
    com::atproto,
    types::{
        string::{AtIdentifier, Datetime, Handle, Nsid},
        BlobRef, LimitedNonZeroU8, TryFromUnknown, TryIntoUnknown, Union,
    },
};
use atrium_xrpc_client::reqwest::ReqwestClient;
use core::str::FromStr;
use feed_rs::parser as feed_parser;
use image::{imageops, GenericImageView, ImageFormat, ImageReader};
use ipld_core::ipld::Ipld::Null;
use rand::seq::SliceRandom;
use regex::Regex;
use reqwest::header;
use scraper::{Html, Selector};
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::io::Cursor;
use tokio::time::{sleep, Duration};

use crate::error::{Error, Result};
use crate::utils;

#[derive(Serialize, Deserialize, Clone, Default, Debug)]
pub struct ExcludeAuthors {
    name: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Default, Debug)]
pub struct Exclude {
    authors: ExcludeAuthors,
}

pub struct NewsBot {
    handle: String,
    password: String,
    session: Option<Session>,
    scrappey_key: Option<String>,
    url: String,
    limit: usize,
    exclude: Exclude,
    posted: HashSet<String>,
}

type MyRecord = bsky::feed::post::Record;

impl NewsBot {
    pub fn new(
        handle: String,
        password: String,
        scrappey_key: Option<String>,
        limit: usize,
        url: String,
        exclude: Exclude,
    ) -> Result<Self> {
        let mut this = Self {
            handle,
            password,
            limit,
            scrappey_key,
            url,
            session: None,
            exclude,
            posted: HashSet::new(),
        };
        Ok(this)
    }
    pub fn get_key(&self, record: &MyRecord) -> Result<String> {
        if let Some(&Union::Refs(bsky::feed::post::RecordEmbedRefs::AppBskyEmbedExternalMain(
            ref main,
        ))) = record.embed.as_ref()
        {
            Ok(main.data.external.uri.to_string())
        } else {
            Err(Error::Any("Invalid Record".into()))
        }
    }
    pub fn is_posted(&self, link: &str) -> Result<bool> {
        Ok(self.posted.contains(link))
    }
    pub async fn post(&mut self, record: MyRecord, dry_run: bool) -> Result<()> {
        let key = self.get_key(&record)?;
        if !self.posted.contains(key.as_str()) {
            let repo = AtIdentifier::Did(
                self.session
                    .clone()
                    .ok_or(Error::Any("Empty session".into()))?
                    .did
                    .clone(),
            );
            let input = atproto::repo::create_record::Input {
                data: atproto::repo::create_record::InputData {
                    collection: Nsid::from_str("app.bsky.feed.post")
                        .map_err(|e| Error::Any(e.to_string()))?,
                    record: record.try_into_unknown()?,
                    repo,
                    rkey: None,
                    swap_commit: None,
                    validate: None,
                },
                extra_data: Null,
            };
            log::info!("input: {:?}", input);
            if !dry_run {
                let agent = self.get_agent().await?;
                agent.api.com.atproto.repo.create_record(input).await?;
                sleep(Duration::from_secs(60)).await;
                self.posted.insert(key);
            }
        }
        Ok(())
    }

    pub async fn execute(&mut self, dry_run: bool) -> Result<()> {
        if self.posted.len() < 100 {
            self.get_previous_posts(1000).await?;
        } else {
            self.get_previous_posts(100).await?;
        }

        let mut posted = 0;
        for link in self.get_links().await? {
            if !self.is_posted(&link)? {
                log::info!("{} found a new RSS feed.", self.handle);
                let record = self.get_record(&link).await?;
                self.post(record, dry_run).await?;
                posted += 1;
                if posted >= self.limit {
                    break;
                }
                self.get_previous_posts(100).await?;
            }
        }
        Ok(())
    }

    pub async fn get_agent(&mut self) -> Result<AtpAgent<MemorySessionStore, ReqwestClient>> {
        let agent = AtpAgent::new(
            ReqwestClient::new("https://bsky.social"),
            MemorySessionStore::default(),
        );
        if let Some(session) = self.session.clone() {
            if agent.resume_session(session.clone()).await.is_ok() {
                self.session = Some(session);
                return Ok(agent);
            }
        }
        let response = match agent.login(&self.handle, &self.password).await {
            Ok(session) => {
                self.session = Some(session);
                Ok(agent)
            }
            Err(err) => {
                log::error!("{:?}", self.handle);
                log::error!("{:?}", err);
                Err(err)
            }
        };
        Ok(response?)
    }
    pub async fn get_previous_posts(&mut self, limit: usize) -> Result<()> {
        let agent = self.get_agent().await?;

        let mut count = 0;
        let mut cursor = None;

        while let Ok(response) = agent
            .api
            .app
            .bsky
            .feed
            .get_author_feed(bsky::feed::get_author_feed::Parameters {
                data: bsky::feed::get_author_feed::ParametersData {
                    actor: AtIdentifier::Handle(
                        Handle::new(self.handle.clone()).map_err(|e| Error::Any(e.to_string()))?,
                    ),
                    cursor: cursor.take(),
                    filter: None,
                    limit: LimitedNonZeroU8::try_from(100).ok(),
                    include_pins: Some(false),
                },
                extra_data: Null,
            })
            .await
        {
            for feed in response.feed.clone() {
                if let Ok(record) = feed::post::Record::try_from_unknown(feed.data.post.data.record)
                {
                    let key = self.get_key(&record)?;
                    log::trace!("{} fetched {}", self.handle, key);
                    self.posted.insert(key);
                }
            }
            cursor.clone_from(&response.cursor);
            let size = response.feed.len();
            count += size;
            log::info!("{} fetched {} posts.", self.handle, count);
            if cursor.is_none() || size == 0 || count >= limit {
                break;
            }
            sleep(Duration::from_secs(3)).await;
        }
        Ok(())
    }
    async fn create_blob(&mut self, image_url: &str) -> Result<BlobRef> {
        let agent = self.get_agent().await?;

        let bytes = reqwest::get(image_url).await?.bytes().await?;
        let img = ImageReader::new(Cursor::new(bytes))
            .with_guessed_format()?
            .decode()?;

        // TODO: dynamic reshaping
        let (width_u, height_u) = img.dimensions();
        let width = f64::from(width_u);
        let height = f64::from(height_u);
        let factor = (400.0 / width).max(300.0 / height);
        let new_width = (width * factor) as u32;
        let new_height = (height * factor) as u32;
        let thumbnail = imageops::thumbnail(&img, new_width, new_height);

        let mut buffer: Vec<u8> = Vec::new();
        thumbnail.write_to(&mut Cursor::new(&mut buffer), ImageFormat::Png)?;

        let response = agent.api.com.atproto.repo.upload_blob(buffer).await?;
        Ok(response.blob.clone())
    }

    pub async fn get_links(&self) -> Result<Vec<String>> {
        let response = {
            let client = reqwest::Client::new();
            let user_agent = [
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.3",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.3",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.1",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.4.1 Safari/605.1.1",
            ].choose(&mut rand::thread_rng()).ok_or(Error::Any("Cannot select user agent".into()))?.to_owned();
            let response = client
                .get(&self.url)
                .header(header::USER_AGENT, user_agent)
                .send()
                .await?;
            response.bytes().await?
        };
        let feed = feed_parser::parse(response.as_ref())?;

        let mut links = Vec::new();
        let re_author_name = self
            .exclude
            .authors
            .name
            .as_ref()
            .map(|pattern| Regex::new(pattern).unwrap());
        for entry in feed.entries {
            let mut exclude = false;
            for person in entry.authors {
                if let Some(re) = &re_author_name {
                    if re.is_match(&person.name) {
                        exclude = true;
                    } else {
                        log::info!("accepted person.name {:?}", person.name);
                    }
                }
            }
            if exclude {
                continue;
            }
            for link in entry.links {
                links.push(link.href);
            }
        }
        Ok(links)
    }
    pub async fn get_record(&mut self, url: &str) -> Result<MyRecord> {
        let html = utils::fetch(url, self.scrappey_key.as_deref()).await?;
        let document = Html::parse_document(html.as_ref());

        let tags = {
            let html_selector = Selector::parse("html").map_err(|e| Error::Any(e.to_string()))?;
            if let Some(html_element) = document.select(&html_selector).next() {
                let mut tags = Vec::new();
                if let Some(class_value) = html_element.value().attr("class") {
                    if class_value.contains("VideoPage") {
                        log::info!("Add video to tags");
                        tags.push("video".to_string());
                    }
                    if class_value.contains("StoryPage") {
                        log::info!("Skip adding story tags");
                    }
                }
                Some(tags)
            } else {
                None
            }
        };

        let title = {
            let selector = Selector::parse("head > meta[property=\"og:title\"]")
                .map_err(|e| Error::Any(e.to_string()))?;
            document
                .select(&selector)
                .next()
                .ok_or(Error::Any(format!("{url} has no meta[property=og:title]")))?
                .value()
                .attr("content")
                .ok_or(Error::Any(format!(
                    "{url} has no content in meta[property=og:title]"
                )))?
                .to_owned()
        };

        let description = {
            let selector = Selector::parse("head > meta[property=\"og:description\"]")
                .map_err(|e| Error::Any(e.to_string()))?;
            document
                .select(&selector)
                .next()
                .ok_or(Error::Any(format!(
                    "{url} has no meta[property=og:description]"
                )))?
                .value()
                .attr("content")
                .ok_or(Error::Any(format!(
                    "{url} has no content in meta[property=og:description]"
                )))?
                .to_owned()
        };
        let image_url = {
            let selector = Selector::parse("head > meta[property=\"og:image\"]")
                .map_err(|e| Error::Any(e.to_string()))?;
            document
                .select(&selector)
                .next()
                .and_then(|meta| {
                    return meta.value().attr("content");
                })
                .map(ToOwned::to_owned)
        };
        let thumb = if let Some(image_url) = image_url {
            self.create_blob(&image_url).await.ok()
        } else {
            None
        };
        let embed = Union::Refs(bsky::feed::post::RecordEmbedRefs::AppBskyEmbedExternalMain(
            Box::new(bsky::embed::external::Main {
                data: bsky::embed::external::MainData {
                    external: bsky::embed::external::External {
                        data: bsky::embed::external::ExternalData {
                            description,
                            thumb,
                            title: title.clone(),
                            uri: url.into(),
                        },
                        extra_data: Null,
                    },
                },
                extra_data: Null,
            }),
        ));
        let text = if let Some(tags) = &tags {
            if !tags.is_empty() {
                format!("{} ({})", title, tags.join(", "))
            } else {
                title
            }
        } else {
            title
        };
        let record = bsky::feed::post::Record {
            data: bsky::feed::post::RecordData {
                text,
                created_at: Datetime::now(),
                embed: Some(embed),
                entities: None,
                facets: None,
                langs: None,
                reply: None,
                labels: None,
                tags,
            },
            extra_data: Null,
        };
        Ok(record)
    }
}
