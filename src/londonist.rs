use std::collections::HashSet;

use crate::error::{Error, Result};
use crate::utils::{fetch, BlueskyBot};
use atrium_api::agent::Session;
use atrium_api::app::bsky;
use atrium_api::types::{string::Datetime, Union};
use feed_rs::parser as feed_parser;
use ipld_core::ipld::Ipld::Null;
use scraper::{Html, Selector};

pub struct LondonistBot {
    handle: String,
    password: String,
    session: Option<Session>,
    limit: usize,
    url: String,
    scrappey_key: Option<String>,
    posted: HashSet<String>,
}

impl BlueskyBot for LondonistBot {
    fn handle(&self) -> &str {
        self.handle.as_str()
    }
    fn password(&self) -> &str {
        self.password.as_str()
    }
    fn session(&self) -> &Option<Session> {
        &self.session
    }
    fn session_mut(&mut self) -> &mut Option<Session> {
        &mut self.session
    }
    async fn execute(&mut self, dry_run: bool) -> Result<()> {
        self.execute_impl(dry_run).await
    }
}

pub struct Article {
    url: String,
}

// Invariant
//    self.key() == Self::from(self.into()).key()
impl Article {
    pub fn key(&self) -> String {
        self.url.to_owned()
    }
    pub fn with(record: &bsky::feed::post::Record) -> Result<Self> {
        if let Some(&Union::Refs(bsky::feed::post::RecordEmbedRefs::AppBskyEmbedExternalMain(
            ref main,
        ))) = record.embed.as_ref()
        {
            Ok(Article {
                url: main.data.external.uri.to_string(),
            })
        } else {
            Err(Error::Any("Invalid Record".into()))
        }
    }
}
impl LondonistBot {
    pub fn new(
        handle: String,
        password: String,
        url: String,
        scrappey_key: Option<String>,
        limit: usize,
    ) -> Self {
        Self {
            handle,
            password,
            url,
            scrappey_key,
            limit,
            session: None,
            posted: HashSet::new(),
        }
    }
    pub async fn fetch_articles(&self) -> Result<Vec<Article>> {
        let rss = fetch(self.url.as_ref(), self.scrappey_key.as_deref()).await?;
        let feed = feed_parser::parse(rss.as_bytes())?;

        let mut articles = Vec::new();
        for entry in feed.entries {
            let url = entry.links.first().unwrap().href.clone();
            articles.push(Article { url });
        }
        articles.reverse();
        Ok(articles)
    }
    pub async fn build_record(&mut self, article: Article) -> Result<bsky::feed::post::Record> {
        let url = article.url.as_ref();
        let html = fetch(url, self.scrappey_key.as_deref()).await?;
        let document = Html::parse_document(html.as_ref());

        let title = {
            let selector = Selector::parse("head > meta[property=\"og:title\"]")
                .map_err(|e| Error::Any(e.to_string()))?;
            document
                .select(&selector)
                .next()
                .ok_or(Error::Any(format!("{url} has no meta[property=og:title]")))?
                .value()
                .attr("content")
                .ok_or(Error::Any(format!(
                    "{url} has no content in meta[property=og:title]"
                )))?
                .to_owned()
        };
        let description = {
            let selector = Selector::parse("head > meta[property=\"og:description\"]")
                .map_err(|e| Error::Any(e.to_string()))?;
            document
                .select(&selector)
                .next()
                .ok_or(Error::Any(format!(
                    "{url} has no meta[property=og:description]"
                )))?
                .value()
                .attr("content")
                .ok_or(Error::Any(format!(
                    "{url} has no content in meta[property=og:description]"
                )))?
                .to_owned()
        };
        let thumb = {
            let selector = Selector::parse("head > meta[property=\"og:image\"]")
                .map_err(|e| Error::Any(e.to_string()))?;
            if let Some(e) = document.select(&selector).next() {
                let image_url = e.value().attr("content").ok_or(Error::Any(format!(
                    "{url} has no content in meta[property=og:description]"
                )))?;
                self.upload_image(image_url).await.ok()
            } else {
                None
            }
        };

        let embed = Union::Refs(bsky::feed::post::RecordEmbedRefs::AppBskyEmbedExternalMain(
            Box::new(bsky::embed::external::Main {
                data: bsky::embed::external::MainData {
                    external: bsky::embed::external::External {
                        data: bsky::embed::external::ExternalData {
                            description,
                            thumb,
                            title: title.to_owned(),
                            uri: url.to_owned(),
                        },
                        extra_data: Null,
                    },
                },
                extra_data: Null,
            }),
        ));
        let text = title;

        let record = bsky::feed::post::Record {
            data: bsky::feed::post::RecordData {
                text,
                created_at: Datetime::now(),
                embed: Some(embed),
                entities: None,
                facets: None,
                langs: None,
                reply: None,
                labels: None,
                tags: None,
            },
            extra_data: Null,
        };
        Ok(record)
    }
    pub async fn execute_impl(&mut self, dry_run: bool) -> Result<()> {
        // Fetch previous posts before the execution
        let limit = if self.posted.len() < 100 { 1000 } else { 100 };
        for record in self.fetch_posted_records(limit).await? {
            let key = Article::with(&record)?.key();
            self.posted.insert(key);
        }

        // Fetch articles to be posted
        let mut count = 0;
        for article in self.fetch_articles().await? {
            let key = article.key();
            if self.posted.contains(&key) {
                continue;
            }
            self.posted.insert(key);
            let record = self.build_record(article).await?;
            self.post(record, dry_run).await?;
            count += 1;
            if count >= self.limit {
                break;
            }
        }
        Ok(())
    }
}
