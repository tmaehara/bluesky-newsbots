extern crate alloc;

use alloc::borrow::ToOwned;
use atrium_api::{
    agent::{store::MemorySessionStore, AtpAgent, Session},
    app::bsky,
    com::atproto,
    types::{
        string::{AtIdentifier, Datetime, Handle, Nsid},
        LimitedNonZeroU8, TryFromUnknown, TryIntoUnknown, Union,
    },
};
use atrium_xrpc_client::reqwest::ReqwestClient;
use core::str::FromStr;
use feed_rs::parser as feed_parser;
use ipld_core::ipld::Ipld::Null;
use rand::seq::SliceRandom;
use reqwest::header;
use scraper::Html;
use std::collections::HashSet;
use tokio::time::{sleep, Duration};

use crate::error::{Error, Result};

pub struct NmccBot {
    handle: String,
    password: String,
    session: Option<Session>,
    limit: usize,
    posted: HashSet<String>,
}

type MyRecord = bsky::feed::post::Record;

impl NmccBot {
    pub fn new(handle: String, password: String, limit: usize) -> Result<Self> {
        Ok(Self {
            handle,
            password,
            limit,
            session: None,
            posted: HashSet::new(),
        })
    }
    pub fn get_key(&self, record: &MyRecord) -> Result<String> {
        if let Some(&Union::Refs(bsky::feed::post::RecordEmbedRefs::AppBskyEmbedExternalMain(
            ref main,
        ))) = record.embed.as_ref()
        {
            Ok(format!("{}::{}", record.text, main.data.external.uri))
        } else {
            Err(Error::Any("Invalid Record".into()))
        }
    }
    pub fn is_posted(&self, record: &bsky::feed::post::Record) -> Result<bool> {
        let key = self.get_key(record)?;
        Ok(self.posted.contains(&key))
    }
    pub async fn post(&mut self, record: MyRecord, dry_run: bool) -> Result<()> {
        let key = self.get_key(&record)?;
        let repo = AtIdentifier::Did(
            self.session
                .clone()
                .ok_or(Error::Any("Empty session".into()))?
                .did
                .clone(),
        );
        let input = atproto::repo::create_record::Input {
            data: atproto::repo::create_record::InputData {
                collection: Nsid::from_str("app.bsky.feed.post")
                    .map_err(|e| Error::Any(e.to_string()))?,
                record: record.try_into_unknown()?,
                repo,
                rkey: None,
                swap_commit: None,
                validate: None,
            },
            extra_data: Null,
        };
        log::info!("input: {:?}", input);
        if !dry_run {
            let agent = self.get_agent().await?;
            agent.api.com.atproto.repo.create_record(input).await?;
            // sleep(Duration::from_secs(60)).await;
            sleep(Duration::from_secs(1)).await;
            self.posted.insert(key);
        }
        Ok(())
    }

    pub async fn execute(&mut self, dry_run: bool) -> Result<()> {
        if self.posted.len() < 100 {
            self.get_previous_posts(1_000).await?;
        } else {
            self.get_previous_posts(100).await?;
        }

        let mut posted = 0;
        for record in self.get_records().await? {
            if !self.is_posted(&record)? {
                log::info!("{} found a new RSS feed.", self.handle);
                self.post(record, dry_run).await?;
                posted += 1;
                if posted >= self.limit {
                    break;
                }
                self.get_previous_posts(100).await?;
            }
        }
        Ok(())
    }

    pub async fn get_agent(&mut self) -> Result<AtpAgent<MemorySessionStore, ReqwestClient>> {
        let agent = AtpAgent::new(
            ReqwestClient::new("https://bsky.social"),
            MemorySessionStore::default(),
        );
        if let Some(session) = self.session.clone() {
            if agent.resume_session(session.clone()).await.is_ok() {
                self.session = Some(session);
                return Ok(agent);
            }
        }
        let response = match agent.login(&self.handle, &self.password).await {
            Ok(session) => {
                self.session = Some(session);
                Ok(agent)
            }
            Err(err) => {
                log::error!("{:?}", self.handle);
                log::error!("{:?}", err);
                Err(err)
            }
        };
        Ok(response?)
    }
    pub async fn get_previous_posts(&mut self, limit: usize) -> Result<()> {
        let agent = self.get_agent().await?;

        let mut count = 0;
        let mut cursor = None;

        while let Ok(response) = agent
            .api
            .app
            .bsky
            .feed
            .get_author_feed(bsky::feed::get_author_feed::Parameters {
                data: bsky::feed::get_author_feed::ParametersData {
                    actor: AtIdentifier::Handle(
                        Handle::new(self.handle.clone()).map_err(|e| Error::Any(e.to_string()))?,
                    ),
                    cursor: cursor.take(),
                    filter: None,
                    limit: LimitedNonZeroU8::try_from(100).ok(),
                    include_pins: Some(false),
                },
                extra_data: Null,
            })
            .await
        {
            for feed in response.feed.clone() {
                if let Ok(record) =
                    bsky::feed::post::Record::try_from_unknown(feed.data.post.data.record)
                {
                    let key = self.get_key(&record)?;
                    log::trace!("{} fetched {}", self.handle, key);
                    self.posted.insert(key);
                }
            }
            cursor.clone_from(&response.cursor);
            let size = response.feed.len();
            count += size;
            log::info!("{} fetched {} posts.", self.handle, count);
            if cursor.is_none() || size == 0 || count >= limit {
                break;
            }
            sleep(Duration::from_secs(3)).await;
        }
        Ok(())
    }
    pub async fn get_records(&self) -> Result<Vec<MyRecord>> {
        const URL: &str = "https://networkmanagementcc.blogspot.com/feeds/posts/default";

        let response = {
            let client = reqwest::Client::new();
            let user_agent = [
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.3",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.3",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.1",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.4.1 Safari/605.1.1",
            ].choose(&mut rand::thread_rng()).ok_or(Error::Any("Cannot select user agent".into()))?.to_owned();
            let response = client
                .get(URL)
                .header(header::USER_AGENT, user_agent)
                .send()
                .await?;
            response.bytes().await?
        };
        let feed = feed_parser::parse(response.as_ref())?;

        let mut records = Vec::new();
        for entry in feed.entries {
            if let Some(content) = entry.content {
                if let Some(body) = content.body {
                    let fragment = Html::parse_fragment(body.as_ref());
                    let body = fragment
                        .tree
                        .into_iter()
                        .flat_map(|node| {
                            if let scraper::node::Node::Text(text) = node {
                                Some(text.text.to_string())
                            } else {
                                None
                            }
                        })
                        .collect::<Vec<_>>()
                        .join("\n");

                    for link in entry.links {
                        if let Some(title) = &link.title {
                            let embed = Union::Refs(
                                bsky::feed::post::RecordEmbedRefs::AppBskyEmbedExternalMain(
                                    Box::new(bsky::embed::external::Main {
                                        data: bsky::embed::external::MainData {
                                            external: bsky::embed::external::External {
                                                data: bsky::embed::external::ExternalData {
                                                    description: body.clone(),
                                                    thumb: None,
                                                    title: title.to_string(),
                                                    uri: link.href.to_string(),
                                                },
                                                extra_data: Null,
                                            },
                                        },
                                        extra_data: Null,
                                    }),
                                ),
                            );
                            let record = bsky::feed::post::Record {
                                data: bsky::feed::post::RecordData {
                                    text: title.to_string(),
                                    created_at: Datetime::now(),
                                    embed: Some(embed),
                                    entities: None,
                                    facets: None,
                                    langs: None,
                                    reply: None,
                                    labels: None,
                                    tags: None,
                                },
                                extra_data: Null,
                            };
                            records.push(record);
                        }
                    }
                }
            }
        }
        Ok(records)
    }
}
