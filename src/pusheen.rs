#![allow(unused)]

use std::collections::HashSet;
use std::io::Cursor;

use crate::error::{Error, Result};
use crate::utils::{fetch, BlueskyBot};
use atrium_api::agent::Session;
use atrium_api::app::bsky;
use atrium_api::types::BlobRef;
use atrium_api::types::{string::Datetime, Union};
use bsky_sdk::rich_text::RichText;
use feed_rs::parser as feed_parser;
use ipld_core::ipld::Ipld::Null;
use rand::seq::SliceRandom;
use reqwest::header;
use scraper::{Html, Selector};
use tokio::io::AsyncReadExt;
use tokio::process::Command;

pub struct PusheenBot {
    handle: String,
    password: String,
    session: Option<Session>,
    limit: usize,
    url: String,
    scrappey_key: Option<String>,
    posted: HashSet<String>,
}

impl BlueskyBot for PusheenBot {
    fn handle(&self) -> &str {
        self.handle.as_str()
    }
    fn password(&self) -> &str {
        self.password.as_str()
    }
    fn session(&self) -> &Option<Session> {
        &self.session
    }
    fn session_mut(&mut self) -> &mut Option<Session> {
        &mut self.session
    }
    async fn execute(&mut self, dry_run: bool) -> Result<()> {
        self.execute_impl(dry_run).await
    }
}

pub struct Article {
    pub url: String,
}

// Invariant
//    self.key() == Self::from(self.into()).key()
impl Article {
    pub fn key(&self) -> String {
        self.url.to_owned()
    }
    pub fn with(record: &bsky::feed::post::Record) -> Result<Self> {
        if let Some(&Union::Refs(bsky::feed::post::RecordEmbedRefs::AppBskyEmbedVideoMain(
            ref main,
        ))) = record.embed.as_ref()
        {
            if let Some(url) = &main.data.alt {
                return Ok(Article {
                    url: url.to_owned(),
                });
            }
        }
        Err(Error::Any("Invalid Record".into()))
    }
}

impl PusheenBot {
    pub fn new(handle: String, password: String, url: String, limit: usize) -> Self {
        Self {
            handle,
            password,
            url,
            scrappey_key: None,
            limit,
            session: None,
            posted: HashSet::new(),
        }
    }
    pub async fn fetch_articles(&self) -> Result<Vec<Article>> {
        let rss = fetch(self.url.as_ref(), self.scrappey_key.as_deref()).await?;
        let feed = feed_parser::parse(rss.as_bytes())?;

        let mut articles = Vec::new();
        for entry in feed.entries {
            let url = entry.links.first().unwrap().href.clone();
            articles.push(Article { url });
        }
        articles.reverse();
        Ok(articles)
    }
    pub async fn build_record(&mut self, article: Article) -> Result<bsky::feed::post::Record> {
        let url = &article.url;

        let client = reqwest::Client::new();

        let user_agent = [
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.3",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.3",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.1",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.4.1 Safari/605.1.1",
        ].choose(&mut rand::thread_rng()).ok_or(Error::Any("Cannot select user agent".into()))?.to_owned();
        let response = client
            .get(url)
            .header(header::USER_AGENT, user_agent)
            .send()
            .await?;

        let html = response.text().await?;
        let document = Html::parse_document(html.as_ref());

        let title = {
            let selector = Selector::parse("title").map_err(|e| Error::Any(e.to_string()))?;
            document
                .select(&selector)
                .next()
                .ok_or(Error::Any(format!("{url} has no title")))?
                .inner_html()
        };

        let gif_url = {
            let selector =
                Selector::parse("dl > dt > a > img").map_err(|e| Error::Any(e.to_string()))?;
            document
                .select(&selector)
                .next()
                .ok_or(Error::Any(format!("{url} has no img")))?
                .value()
                .attr("src")
                .ok_or(Error::Any(format!("{url} has invalid img")))?
                .to_owned()
        };
        log::info!("process {} with {}", url, gif_url);
        let blob_ref = self.upload_video(&gif_url).await?;
        let embed = Union::Refs(bsky::feed::post::RecordEmbedRefs::AppBskyEmbedVideoMain(
            Box::new(bsky::embed::video::Main {
                data: bsky::embed::video::MainData {
                    alt: Some(url.to_string()),
                    aspect_ratio: None,
                    captions: None,
                    video: blob_ref.clone(),
                },
                extra_data: Null,
            }),
        ));

        let rt = RichText::new_with_detect_facets(url).await.unwrap();
        let record = bsky::feed::post::Record {
            data: bsky::feed::post::RecordData {
                text: rt.text,
                facets: rt.facets,
                created_at: Datetime::now(),
                embed: Some(embed),
                entities: None,
                langs: None,
                reply: None,
                labels: None,
                tags: None,
            },
            extra_data: Null,
        };
        Ok(record)
    }
    pub async fn execute_impl(&mut self, dry_run: bool) -> Result<()> {
        // Fetch previous posts before the execution
        let limit = if self.posted.len() < 100 { 1000 } else { 100 };
        for record in self.fetch_posted_records(limit).await? {
            let key = Article::with(&record)?.key();
            self.posted.insert(key);
        }
        // Fetch articles to be posted
        let mut count = 0;
        for article in self.fetch_articles().await? {
            let key = article.key();
            if self.posted.contains(&key) {
                continue;
            }
            self.posted.insert(key);
            let record = self.build_record(article).await?;
            self.post(record, dry_run).await?;
            count += 1;
            if count >= self.limit {
                break;
            }
        }
        Ok(())
    }
}
