#![allow(unused_variables)]
#![allow(unused_mut)]

extern crate alloc;

use alloc::borrow::ToOwned;
use atrium_api::{
    agent::Session,
    app::bsky,
    types::{string::Datetime, Union},
};
use feed_rs::parser as feed_parser;
use ipld_core::ipld::Ipld::Null;

use crate::{
    error::{Error, Result},
    utils::{fetch, BlueskyBot},
};
use std::collections::HashSet;

pub struct FacebookBot {
    handle: String,
    password: String,
    session: Option<Session>,
    url: String,
    limit: usize,
    pub posted: HashSet<String>,
}

impl BlueskyBot for FacebookBot {
    fn handle(&self) -> &str {
        self.handle.as_str()
    }
    fn password(&self) -> &str {
        self.password.as_str()
    }
    fn session(&self) -> &Option<Session> {
        &self.session
    }
    fn session_mut(&mut self) -> &mut Option<Session> {
        &mut self.session
    }
    async fn execute(&mut self, dry_run: bool) -> Result<()> {
        self.execute_impl(dry_run).await
    }
}

pub struct Article {
    title: String,
    url: String,
    image_url: Option<String>,
}

impl Article {
    pub fn key(&self) -> String {
        self.url.to_owned()
    }
    pub fn with(record: &bsky::feed::post::Record) -> Result<Self> {
        if let Some(&Union::Refs(bsky::feed::post::RecordEmbedRefs::AppBskyEmbedExternalMain(
            ref main,
        ))) = record.embed.as_ref()
        {
            Ok(Article {
                title: main.data.external.title.to_string(),
                url: main.data.external.uri.to_string(),
                image_url: None,
            })
        } else {
            Err(Error::Any("Invalid Record".into()))
        }
    }
}

impl FacebookBot {
    pub fn new(handle: String, password: String, url: String, limit: usize) -> Self {
        Self {
            handle,
            password,
            url,
            limit,
            session: None,
            posted: HashSet::new(),
        }
    }

    pub async fn fetch_articles(&self) -> Result<Vec<Article>> {
        let rss = fetch(self.url.as_ref(), None).await?;
        let feed = feed_parser::parse(rss.as_bytes())?;

        let mut articles = Vec::new();
        for entry in feed.entries {
            let title = entry.title.unwrap().content.clone();
            let url = entry.links.first().unwrap().href.clone();
            let mut image_url = None;
            for media in entry.media.iter() {
                if let Some(content) = media.content.first() {
                    if let Some(url) = &content.url {
                        image_url = Some(url.to_string());
                        break;
                    }
                }
            }
            articles.push(Article {
                title,
                url,
                image_url,
            })
        }
        articles.reverse();
        Ok(articles)
    }

    pub async fn build_record(&mut self, article: Article) -> Result<bsky::feed::post::Record> {
        let thumb = if let Some(image_url) = &article.image_url {
            self.upload_image(image_url).await.ok()
        } else {
            None
        };
        let embed = Union::Refs(bsky::feed::post::RecordEmbedRefs::AppBskyEmbedExternalMain(
            Box::new(bsky::embed::external::Main {
                data: bsky::embed::external::MainData {
                    external: bsky::embed::external::External {
                        data: bsky::embed::external::ExternalData {
                            description: "".to_string(),
                            thumb,
                            title: article.title.clone(),
                            uri: article.url.clone(),
                        },
                        extra_data: Null,
                    },
                },
                extra_data: Null,
            }),
        ));
        let text = article.title.clone();
        let record = bsky::feed::post::Record {
            data: bsky::feed::post::RecordData {
                text,
                created_at: Datetime::now(),
                embed: Some(embed),
                entities: None,
                facets: None,
                langs: None,
                reply: None,
                labels: None,
                tags: None,
            },
            extra_data: Null,
        };
        Ok(record)
    }

    pub async fn execute_impl(&mut self, dry_run: bool) -> Result<()> {
        let limit = if self.posted.len() < 100 { 1000 } else { 100 };
        for record in self.fetch_posted_records(limit).await? {
            let key = Article::with(&record)?.key();
            self.posted.insert(key);
        }

        // Fetch articles to be posted
        let mut count = 0;
        for article in self.fetch_articles().await? {
            let key = article.key();
            if self.posted.contains(&key) {
                continue;
            }
            self.posted.insert(key);
            let record = self.build_record(article).await?;
            self.post(record, dry_run).await?;
            count += 1;
            if count >= self.limit {
                break;
            }
        }
        Ok(())
    }
}
