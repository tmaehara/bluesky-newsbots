#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    VarError(#[from] std::env::VarError),

    #[error("Scrappey Error `{0}`")]
    Scrappey(String),

    #[error("Any Error `{0}`")]
    Any(String),

    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),

    #[error(transparent)]
    AtriumError(#[from] atrium_api::error::Error),

    #[error(transparent)]
    AtriumCreateSession(
        #[from] atrium_xrpc::error::Error<atrium_api::com::atproto::server::create_session::Error>,
    ),
    #[error(transparent)]
    AtriumCreateRecord(
        #[from] atrium_xrpc::Error<atrium_api::com::atproto::repo::create_record::Error>,
    ),
    #[error(transparent)]
    AtriumUploadBlob(
        #[from] atrium_xrpc::Error<atrium_api::com::atproto::repo::upload_blob::Error>,
    ),

    #[error(transparent)]
    FeedFsParser(#[from] feed_rs::parser::ParseFeedError),

    #[error(transparent)]
    Serde(#[from] serde_json::Error),

    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[error(transparent)]
    Image(#[from] image::ImageError),
    // #[error(transparent)]
    // Select(#[from] SelectorErrorKind<'_>),
    /*
    #[error(transparent)]
    Sqlx(#[from] sqlx::Error),

    #[error(transparent)]
    Env(#[from] std::env::VarError),

    #[error(transparent)]
    Parse(#[from] core::num::ParseIntError),
    */
}
pub type Result<T> = std::result::Result<T, Error>;
