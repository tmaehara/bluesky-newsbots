FROM rust:latest AS builder

WORKDIR /usr/src/app

COPY Cargo.lock Cargo.lock
COPY Cargo.toml Cargo.toml
COPY src src
RUN cargo build --release

FROM debian:bookworm-slim

RUN apt-get update && apt-get install -y --no-install-recommends ca-certificates ffmpeg && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/src/app/target/release/bluesky-newsbots /usr/local/bin/bluesky-newsbots
ENV RUST_LOG=info

CMD ["bluesky-newsbots"]
