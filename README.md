# Bluesky-Newsbots

Bots that fetch sources (mainly RSS feeds) and repost to Bluesky.

# Running Bots

## Active

The [author](https://bsky.app/profile/tmaehara.bsky.social) is running the following bots. They are tentative solution until the official accounts join to Bluesky. 

- [Guardian (UK)](https://bsky.app/profile/guardian-uk-rss.bsky.social)
- [BBC News (World)](https://bsky.app/profile/bbcnews-world-rss.bsky.social)
- [BBC News (UK)](https://bsky.app/profile/bbcnews-uk-rss.bsky.social)
- [AP News (World)](https://bsky.app/profile/apnews-world-rss.bsky.social)
- [Toronto Star (All News stories)](https://bsky.app/profile/torontostar-rss.bsky.social)
- [Timeout London](https://bsky.app/profile/timeout-london-rss.bsky.social)
- [Daily Squib](https://bsky.app/profile/dailysquib-rss.bsky.social)
- [Londonist](https://bsky.app/profile/londonist-rss.bsky.social)
- [Ealing Times](https://bsky.app/profile/ealingtimes-rss.bsky.social)
- [NMCC Bus Network Information Dashboard](https://bsky.app/profile/nmcc-rss.bsky.social)
- [Cardiff Bus](https://bsky.app/profile/cardiffbus-bot.bsky.social)
- [Pusheen The Cat](https://bsky.app/profile/pusheen-bot.bsky.social)

Please feel free to ask me adding any bots. (Note: Scraping from X and Facebook is not allowed by their Term of Service.)

## Archived

The following bots completed their mission since the official accounts have joined. 

- [Guardian (World)](https://bsky.app/profile/guardian-world-rss.bsky.social)
- [Reuters (World)](https://bsky.app/profile/reuters-world-rss.bsky.social)


# Buy Me a Coffee

As mentioned the above, now the author is operating a few bots on my local kubernetes cluster. The operational cost is about 5 GBP/month. If you can afford it, please support me via [Buy Me a Coffee](https://buymeacoffee.com/tmaehara).

